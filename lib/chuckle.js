var paul = require('paulchuckle');
var barry = require('barrychuckle');
var fs = require('fs');

var brothers = {

    notifiers: {
        'console': require('./notifiers/console')
    },

    chuckle: function(filename) {

        var _this = this;

        var data = fs.readFileSync(filename, 'utf8');
        var config = JSON.parse(data);

        if (config.paul) {
            console.log('Starting Paul, To me.');
            paul.start(config.paul.port);
        }

        if (config.barry) {
            for(var b in config.barry) {
                console.log('Starting Barry[' + b + '], To you.');
                this.barry(config.barry[b]);
            }
        }
    },
    barry: function(config) {

        if (!config.notifiers) {
            return;
        }

        var _this = this;
        var delay = (config.seconds || 60) * 1000;

        setInterval(function() {
            barry.chuckle(config.endpoint, function(success, res) {

                var notifierConfig, notifier, method;

                for(var n in config.notifiers) {
                    notifierConfig = config.notifiers[n];
                    notifier = _this.notifiers[notifierConfig.type];

                    if (notifier) {
                        method = success ? 'onSuccess' : 'onError';
                        notifier[method](config, notifierConfig, res);
                    }
                }
            });
        }, delay);
    }
};

module.exports = {
    chuckle: function(filename) {
        brothers.chuckle(filename);
    }
};
