module.exports = {
    onSuccess: function(config, notifierConfig, res) {
        if (!notifierConfig['only-errors']) {
            console.log(config.name + ' responded successfully with ' + JSON.stringify(res));
        }
    },
    onError: function(config, notifierConfig, res) {
        console.error(config.name + ' failed with ' + JSON.stringify(res));
    }
};
