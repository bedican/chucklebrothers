# ChuckleBrothers

ChuckleBrothers is a simple monitor to test the upstate of a server.

Why? Humour! But hey, its functional too :)

## Installation

``` bash
$ npm install -g chucklebrothers
```

## Usage

```bash
# To me.. to you
$ chuckle
```

## Configuration

The ChuckleBrothers are configured with the file `chuckle.json` in the current working directory.

### Example `chuckle.json`

```json
{
    "barry": [
        {
            "name": "local",
            "endpoint": "http://127.0.0.1:3101",
            "seconds": 60,
            "notifiers": [
                {
                    "type": "console",
                    "only-errors": false
                }
            ]
        }
    ],
    "paul": {
        "port": 3101
    }
}
```

### Options

#### Barry

[BarryChuckle](https://www.npmjs.com/package/barrychuckle) is the client component, and as we love Barry, there can be many.

| Option    | Description                              |
| --------- | ---------------------------------------- |
| name      | A descriptive name for Barry.            |
| endpoint  | Where to find Paul.                      |
| seconds   | How long between Barry asking Paul.      |
| notifiers | Who to notify when Barry and Paul speak. |

##### Notifiers

Currently there is only one notifier type, the console. Example configuration shown above.

#### Paul

[PaulChuckle](https://www.npmjs.com/package/paulchuckle) is the server component, and as we love Paul, there can only be one and only.

| Option    | Description              |
| --------- | ------------------------ |
| port      | Where Paul is listening. |

#### License: MIT
#### Author: [Ash Brown](https://bitbucket.org/bedican)
